<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <script src="https://use.fontawesome.com/64fa1dc8a7.js"></script>

    <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href='https://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <style>
        body{
            background-color:black;
            background-image: url("assets/img/back.jpg");
            background-size:cover;
            background-repeat: no-repeat;
            background-position: fixed;

        }
        .card{
            margin:0px 0px !important;

        }
        .row .col{
            width: auto !important;
        }

        .custom-img{

            padding:0px 0px !important;
            border: 1px #ffffff solid;
            max-width:600px;
            max-height:600px;

        }
        input[type=file]{
            color:#ffffff !important;
        }
        .text{
            color:white;
        }
        .custom-img-300x300{
            padding:0px 0px !important;
            border: 1px #ffffff solid;
            max-width:300px;
            max-height:300px;
        }
        .custom-img-1050x700{
            padding:0px 0px !important;
            border: 1px #ffffff solid;
            max-width:1050px;
            max-height:700px;
        }
    </style>
    <title>Home</title>
</head>
<body>
<div class="container">

    <div class="row">
        <div class="col l12 center-align">

            {{ Form::open(array('role'=>'form','id'=>'something','files'=>true)) }}
            <div class="file-field input-field ">
                <div class="btn waves-effect waves-light red small" >
                    <span>Choose an Image</span>
                    {{ Form::file('image') }}
                </div>

            </div>
            <div class="input-field">

                {{ Form::submit("Upload",["class"=>"btn red"]) }}
            </div>

            {{ Form::close() }}

        </div>
    </div>
    <div class="row">
        <div class="col l12 center-align">
            <div class="flow-text text ">Resize 1050x700</div>
        </div>
    </div>
    <div class="row">
        <div class="col m12 l12 s12 center-align ">
            <img src="test-1050x700.png" alt="" class="responsive-img custom-img-1050x700 ">
        </div>
    </div>
    <div class="row">
        <div class="col m12 center-align">
            <div class="flow-text text">Crop 300x300</div>
        </div>
    </div>
    <div class="row ">

        <div class="col m12 l3 s12 custom-img-300x300">

            <img src="test-300x300.png" alt="Testing" class="responsive-img">

        </div>
        <div class="col m12 l3 s12 custom-img-300x300">

            <img src="test-300x300.png" alt="Testing" class="responsive-img">


        </div>
        <div class="col m12 l3 s12 custom-img-300x300">


            <img src="test-300x300.png" alt="Testing" class="responsive-img">


        </div>
        <div class="col m12 l3 s12 custom-img-300x300">

            <img src="test-300x300.png" alt="Testing" class="responsive-img">


        </div>

    </div>
    <div class="row">
        <div class="col m12 center-align">
            <div class="flow-text text">Crop 600x600</div>
        </div>
    </div>
    <div class="row">

        <div class="col m12 l3 s12 custom-img">

            <img src="test-600x600.png" alt="Testing" class="responsive-img">


        </div>
        <div class="col m12 l3 s12 custom-img">

            <img src="test-600x600.png" alt="Testing" class="responsive-img">

        </div>
        <div class="col m12 l3 s12 custom-img">

            <img src="test-600x600.png" alt="Testing" class="responsive-img">

        </div>
        <div class="col m12 l3 s12 custom-img">

            <img src="test-600x600.png" alt="Testing" class="responsive-img">

        </div>

    </div>

</div>
<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>

</body>
</html>
